//入口文件
import Vue from 'vue'

//导入 App 组件包
import App from './App.vue'

//导入 Mint-UI 包
import {
  Header,
  Swipe,
  SwipeItem,
  Button
} from 'mint-ui'
//手动注册
Vue.component(Header.name, Header)
Vue.component(Swipe.name, Swipe);
Vue.component(SwipeItem.name, SwipeItem);
Vue.component(Button.name, Button);

//导入 mui 包
import './lib/mui/css/mui.min.css'
import './lib/mui/css/icons-extra.css'

//导入 VueRouter 包
import VueRouter from 'vue-router'
Vue.use(VueRouter)

//导入 router.js
import router from './router.js'

//导入 vue-resource 包
import VueResource from 'vue-resource'
Vue.use(VueResource)
Vue.http.options.root = 'http://www.liulongbin.top:3005'

//导入 moment 包
import moment from 'moment'
//定义全局过滤器
Vue.filter('dateFormat', function (dateStr, pattern = "YYYY-MM-DD HH:mm:ss") {
  return moment(dateStr).format(pattern)
})

var vm = new Vue({
  el: '#app',
  data: {},
  methods: {},
  render: c => c(App),
  router
})