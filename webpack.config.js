const path = require('path')

//在内存中，根据指定的模板页面生成一份内存中的首页，同时自动把打包好的 bundle 注入到页面底部
//如果要配置插件，需要在导出的对象中挂载一个 plugin 节点
const htmlWebpackPlugin = require('html-webpack-plugin')

const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {
  entry: './src/main.js',
  output: { //指定输出选项
    path: path.resolve(__dirname, 'dist'), //入口文件
    filename: 'bundle.js' //指定输出文件的名称
  },
  mode: 'development',
  plugins: [ //所有 webpack 插件的配置节点
    new htmlWebpackPlugin({
      template: path.resolve(__dirname, './src/index.html'), //指定模板文件路径
      filename: 'index.html' //设置生成的内存页面的名称
    }),
    new VueLoaderPlugin()
  ],
  module: { //配置所有第三方 loader 模块的
    rules: [ //第三方模块的匹配规则
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(jpg|png|gif|bmp|jpeg)$/,
        use: 'url-loader?limit=80720&name=[hash:8]-[name].[ext]' // limit 给定的值是图片的大小，单位是 byte ，如果要引用的图片大于或等于给定的limit值，则不会被转为 base64 格式的字符串。如果图片小于给定的limit值，则会被转为 base64 的字符串
      }, //处理图片路径的 loader
      {
        test: /\.(ttf|eot|svg|woff|woff2|otf)$/,
        use: 'url-loader'
      }, //处理字体文件的 loader
      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: /node_modules/
      }, //配置 babel 来转换高级的 ES 语法
      {
        test: /\.vue$/,
        use: 'vue-loader'
      }
    ]
  },
  resolve: {
    alias: { //修改 Vue 被导入时候的包的路径
      //"vue$": "vue/dist/vue.js"
    }
  }
}